package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

@Repository
public class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    @Override
    public void add(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (userId == null || userId.isEmpty()) return;
        if (project == null) return;
        project.setUserId(userId);
        entityManager.persist(project);
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p WHERE p.id = :id", ProjectDto.class).
                setParameter("id", id).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        List<ProjectDto> resultList = entityManager.
                createQuery("SELECT p FROM ProjectDto p WHERE p.id = :id AND p.userId = :userId", ProjectDto.class).
                setParameter("id", id).
                setParameter("userId", userId).
                setMaxResults(1).getResultList();
        return getFirstRecord(resultList);

    }

    @NotNull
    @Override
    public List<ProjectDto> findAll() {
        return entityManager.createQuery("FROM ProjectDto", ProjectDto.class).
                setHint("org.hibernate.cacheable", true).
                getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p WHERE p.userId = :userId", ProjectDto.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("userId", userId).
                getResultList();
    }

    @NotNull
    public List<ProjectDto> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p ORDER BY :sort DESC", ProjectDto.class).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p ORDER BY :sort DESC", ProjectDto.class).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p WHERE p.userId = :userId ORDER BY :sort DESC", ProjectDto.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM ProjectDto p WHERE p.userId = :userId ORDER BY :sort DESC", ProjectDto.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM ProjectDto p WHERE p.id = :id", Boolean.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM ProjectDto p WHERE p.id = :id AND userId = :userId", Boolean.class).
                setParameter("userId", userId).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDto p", Long.class).getSingleResult();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT COUNT(p) FROM ProjectDto p WHERE p.userId = :userId", Long.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Override
    public void remove(@Nullable final ProjectDto project) {
        entityManager.remove(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (project == null) return;
        @Nullable final ProjectDto repositoryProject = this.findById(userId, project.getId());
        if (repositoryProject == null) return;
        entityManager.remove(repositoryProject);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final ProjectDto project = findById(id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final ProjectDto repositoryProject = this.findById(userId, id);
        if (repositoryProject == null) return;
        entityManager.remove(repositoryProject);
    }

    @Override
    public void clear() {
        for (@NotNull final ProjectDto project : findAll()) {
            entityManager.remove(project);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        for (@NotNull final ProjectDto project : findAll(userId)) {
            entityManager.remove(project);
        }
    }

}
