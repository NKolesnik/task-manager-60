package ru.t1consulting.nkolesnik.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDto;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDto, R extends IDtoRepository<M>> implements IDtoService<M> {

}
