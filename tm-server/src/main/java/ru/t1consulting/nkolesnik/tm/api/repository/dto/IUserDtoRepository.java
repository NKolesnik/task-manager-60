package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

    void setPassword(@Nullable UserDto user, @Nullable String password);

    void removeByLogin(@Nullable String login);

}
