package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDto;

import java.util.List;

public interface IDtoRepository<M extends AbstractModelDto> {

    void add(@Nullable M modelDTO);

    long getSize();

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@Nullable String id);

    boolean existsById(@Nullable String id);

    void update(@NotNull M modelDTO);

    void clear();

    void remove(@Nullable M modelDTO);

    void removeById(@Nullable String id);

}
