package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends IDtoRepository<M> {

    void add(@Nullable String userId, @Nullable M modelDTO);

    long getSize(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    void remove(@Nullable String userId, @Nullable M modelDTO);

    void removeById(@Nullable String userId, @Nullable String id);

}
