package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserDtoRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public final class UserDtoService extends AbstractDtoService<UserDto, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    protected IUserDtoRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @Override
    @Transactional
    public void add(@Nullable final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        repository.add(user);
    }

    @Override
    @Transactional
    public void addALl(@Nullable final Collection<UserDto> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        for (UserDto user : users)
            repository.add(user);
    }

    @Override
    @Transactional
    public void set(@Nullable final Collection<UserDto> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        repository.clear();
        for (UserDto user : users)
            repository.add(user);
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    public List<UserDto> findAll() {
        @Nullable final List<UserDto> users;
        users = repository.findAll();
        if (users.isEmpty()) return Collections.emptyList();
        return users;
    }

    @Nullable
    @Override
    public UserDto findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return repository.findById(id);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        repository.removeById(id);
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setRole(Role.USUAL);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setRole(Role.USUAL);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (role == null) throw new RoleIsEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setRole(Role.USUAL);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setRole(Role.USUAL);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new UserNotFoundException();
        repository.setPassword(user, password);
    }

    @NotNull
    @Override
    @Transactional
    public UserDto updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @Nullable final UserDto result;
        result = repository.findById(id);
        if (result == null) throw new UserNotFoundException();
        repository.updateUser(id, firstName, middleName, lastName);
        return result;
    }

    @Override
    @Transactional
    public void update(@Nullable final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        repository.update(user);
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        @Nullable final UserDto result;
        result = repository.findById(user.getId());
        if (result == null) throw new UserNotFoundException();
        @NotNull final String userId = result.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        repository.remove(result);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        repository.removeByLogin(login);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto repositoryUser = repository.findByLogin(login);
        if (repositoryUser == null) throw new UserNotFoundException();
        repository.lockUserByLogin(login);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto repositoryUser = repository.findByLogin(login);
        if (repositoryUser == null) throw new UserNotFoundException();
        repository.unlockUserByLogin(login);
    }

}
