package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.List;

@Repository
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends
        AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @Override
    public abstract void add(@Nullable String userId, @Nullable M modelDto);

    @Override
    public abstract long getSize(@Nullable String userId);

    @NotNull
    @Override
    public abstract List<M> findAll(@Nullable String userId);

    @Nullable
    @Override
    public abstract M findById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract void clear(@Nullable String userId);

    @Override
    public abstract void remove(@Nullable String userId, @Nullable M modelDto);

    @Override
    public abstract void removeById(@Nullable String userId, @Nullable String id);

}
