package ru.t1consulting.nkolesnik.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

@Repository
public class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDto task) {
        if (userId == null || userId.isEmpty()) return;
        if (task == null) return;
        task.setUserId(userId);
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String id) {
        List<TaskDto> resultList = entityManager.
                createQuery("SELECT t FROM TaskDto t WHERE t.id = :id", TaskDto.class).
                setParameter("id", id).
                setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        List<TaskDto> resultList = entityManager.
                createQuery("SELECT t FROM TaskDto t WHERE t.id = :id AND t.userId = :userId", TaskDto.class).
                setParameter("id", id).
                setParameter("userId", userId).
                setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        return entityManager.createQuery("FROM TaskDto", TaskDto.class).
                setHint("org.hibernate.cacheable", true).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDto t WHERE t.userId = :userId", TaskDto.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("userId", userId).
                getResultList();
    }

    @NotNull
    public List<TaskDto> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDto t ORDER BY :sort DESC", TaskDto.class).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<TaskDto> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDto t ORDER BY :sort DESC", TaskDto.class).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    public List<TaskDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDto t WHERE t.userId = :userId ORDER BY :sort DESC", TaskDto.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<TaskDto> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDto t WHERE t.userId = :userId ORDER BY :sort DESC", TaskDto.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDto t " +
                        "WHERE t.userId = :userId AND t.projectId = :projectId", TaskDto.class).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDto t " +
                        "WHERE t.userId = :userId AND t.projectId = :projectId", TaskDto.class).
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM TaskDto t WHERE t.id = :id", Boolean.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM TaskDto t WHERE t.id = :id AND userId = :userId", Boolean.class).
                setParameter("userId", userId).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDto t", Long.class).getSingleResult();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT COUNT(t) FROM TaskDto t WHERE t.userId = :userId", Long.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Override
    public void remove(@Nullable final TaskDto task) {
        entityManager.remove(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final TaskDto task) {
        if (task == null) return;
        @Nullable final TaskDto repositoryTask = this.findById(userId, task.getId());
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final TaskDto task = findById(id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final TaskDto repositoryTask = this.findById(userId, id);
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeByProjectId(@Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM TaskDto t WHERE t.projectId = :projectId").
                setParameter("projectId", projectId).
                executeUpdate();
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM TaskDto t WHERE t.projectId = :projectId AND t.userId = :userId").
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public void clear() {
        for (@NotNull final TaskDto task : findAll()) {
            entityManager.remove(task);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        for (@NotNull final TaskDto task : findAll(userId)) {
            entityManager.remove(task);
        }
    }
}
