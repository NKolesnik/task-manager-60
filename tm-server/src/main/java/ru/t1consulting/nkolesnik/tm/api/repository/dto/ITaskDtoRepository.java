package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    List<TaskDto> findAll(@Nullable Sort sort);

    List<TaskDto> findAll(@Nullable String userId, @Nullable Sort sort);

    List<TaskDto> findAll(@Nullable Comparator comparator);

    List<TaskDto> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String projectId);

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeByProjectId(@Nullable String projectId);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
